/*
   Remote control via telnet for Linpac
   (c) 1999 - 2000 by Radek Burget OK2JBG

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version
   2 of the license, or (at your option) any later version.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <utime.h>
#include <errno.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "lpapp.h"
#include "lpcalls.h"

#define LINE_LEN 80
#define BUFSIZE 256

#define ABORT_ADDR "ipcontrol"
#define SOCKTIME 100

int abort_all = 0;         /* 1 = abort event was received */

int channels = 0;          // number of channels

int *sock;                 // telnet socket for each channel [chn]
int list;                  // listening socket
int maxsock;               // highest socket number
int portnum;
char mycall[20];
char **line;               //for line recieving [chn][char]
char **sline;              //for line sending [chn][char]
bool *login;               //user is logging in [chn]

//-------------------------------------------------------------------------

/* fgets_cr - works like fgets, but uses CR as EOLN */
char *fgets_cr(char *s, int size, FILE *stream)
{
  int cnt = 0;
  int ch;
  while (cnt < size)
  {
    do ch = fgetc(stream); while (ch == EOF && errno == EINTR);
    if (ch == EOF || abort_all) {s[cnt++] = '\0'; break;}
    s[cnt++] = ch;
    if (ch == '\r') {s[cnt++] = '\0'; break;}
  }
  return s;
}

/* output the data with CR->LF conversion */
int data_output(int chn, const char *data, int len)
{
    char *buf = new char[len];
    char *p = (char *)data;
    char *q = buf;
    for (int i = 0; i < len; i++)
    {
        if (*p == '\r') *q = '\n';
        else *q = *p;
        p++; q++;
    }
    int res = write(sock[chn], buf, len);
    delete[] buf;
    return res;
}

/* input the data with LF->CR conversion */
int data_input(int chn, char *data, int len)
{
    char *buf = new char[len];
    int res = read(sock[chn], buf, len);
    if (res == 0) login[chn] = false; //end of file - do not continue logging in

    char *p = buf;
    char *q = data;
    int vlen = 0;
    for (int i = 0; i < res; i++)
    {
        if (*p == '\n') *q = '\r';
        else if (*p == '\r') { p++; continue; }
        else *q = *p;
        p++; q++; vlen++;
    }
    delete[] buf;
    return vlen;
}

void set_channel_state(int chn, int state)
{
   char s[32];
   sprintf(s, "%i", state);
   lp_set_var(chn, "_state", s);
}

//-------------------------------------------------------------------------

void status()
{
    int ncon = 0;
    int nlog = 0;
    for (int i = 1; i < channels; i++)
    {
        if (sock[i] != -1 && !login[i]) ncon++;
        if (login[i]) nlog++;
    }
    lp_statline("IPCONTROL for %s : %i connected : %i logging in",
             mycall, ncon, nlog);
}

//-------------------------------------------------------------------------

/* create the inet address from IP address an port number */
void create_address(unsigned short port, unsigned char ip1,
                                         unsigned char ip2,
                                         unsigned char ip3,
                                         unsigned char ip4,
                                         struct sockaddr_in *addr)
{
  addr->sin_family = AF_INET;
  addr->sin_port = htons(port);
  addr->sin_addr.s_addr = ((ip4*256 + ip3)*256 + ip2)*256 + ip1;
}

/* create the inet address from INADDR_ANY and port number */
void create_address_any(unsigned short port, struct sockaddr_in *addr)
{
  addr->sin_family = AF_INET;
  addr->sin_port = htons(port);
  addr->sin_addr.s_addr = INADDR_ANY;
}

/* create new listening socket */
int init_socket(int port)
{
  int sock;
  struct sockaddr_in addr;
  sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock == -1)
  {
      perror("Cannot create socket");
      return -1;
  }

  create_address_any(port, &addr);
  if (bind(sock, (struct sockaddr*)&addr, sizeof(struct sockaddr)) == -1)
  {
      perror("Cannot bind()");
      return -1;
  }

  int i = 1;
  setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &i, sizeof(int));

  if (listen(sock, 0) == -1)
  {
      perror("cannot listen()");
      return -1;
  }
  return sock;
}

/* Find first free channel with specified callsign */
int find_free_channel(char *call)
{
   for (int i = 1; i < channels; i++)
       if (sock[i] == -1 && lp_chn_status(i) == ST_DISC &&
           compare_call_ssid(call, lp_chn_call(i))) return i;
   return -1;
}

/* Provides the login dialog */
void invite_user(int chn)
{
  FILE *io = fdopen(sock[chn], "w+");
  if (io == NULL)
  {
    fprintf(stderr, "ipcontrol: Cannot create io file\n");
    return;
  }
  else
  {
    login[chn] = true;
    status();
    setbuf(io, NULL);
    fprintf(io, "\nHello, this is %s telnet access\n\n", mycall);
    fprintf(io, "Please enter your callsign: ");
    set_channel_state(chn, ST_CONP);
    lp_emit_event(chn, EV_STATUS_CHANGE, ST_CONP, NULL);
  }
}

/* disconnect the session */
void disconnect(int chn)
{
    char call[20];
    strcpy(call, lp_chn_cphy(chn));
    
    set_channel_state(chn, ST_DISC);
    lp_emit_event(chn, EV_STATUS_CHANGE, ST_DISC, NULL);
    lp_emit_event(chn, EV_DISC_FM, 0, call);
    lp_set_var(chn, "_cwit", "");
    lp_set_var(chn, "_cphy", "");
    close(sock[chn]);
    sock[chn] = -1;
    status();

    lp_del_var(chn, "IPADDR");
}

/* wait for data and handle them */
void wait_data()
{
    fd_set rfds;
    struct timeval tv;
    int rc;
  
    FD_ZERO(&rfds);
    FD_SET(list, &rfds); //listening socket
    for (int i = 0; i < channels; i++) //connection socket
        if (sock[i] != -1) FD_SET(sock[i], &rfds);
  
    tv.tv_sec = 10;
    tv.tv_usec = 0;
    rc = select(maxsock+1, &rfds, NULL, NULL, &tv);
    if (rc > 0)
    {
        if (FD_ISSET(list, &rfds)) //check listening socket
        {
            struct sockaddr_in addr;
            socklen_t len = sizeof(struct sockaddr);
            int newsock = accept(list, (sockaddr *)&addr, &len);

            int newchn = find_free_channel(mycall);
            if (newchn > 0)
            {
                sock[newchn] = newsock;
                if (newsock > maxsock) maxsock = newsock;

                char ipaddr[20];
                sprintf(ipaddr, "%i.%i.%i.%i",
                        addr.sin_addr.s_addr%256,
                        (addr.sin_addr.s_addr/256)%256,
                        (addr.sin_addr.s_addr/256/256)%256,
                        addr.sin_addr.s_addr/256/256/256);
                lp_set_var(newchn, "IPADDR", ipaddr);
                
                invite_user(newchn);
            }
            else
            {
                char sorry[] = "\nHi, this is LinPac telnet access\n"
                               "There is no free channel to connect to.\n"
                               "Please try later.\n\n";
                write(newsock, sorry, strlen(sorry));
                close(newsock);
            }
        }

        //check connected sockets
        for (int chn = 1; chn < channels; chn++)
            if (sock[chn] != -1 && FD_ISSET(sock[chn], &rfds))
            {
                char buf[BUFSIZE];
                int cnt = data_input(chn, buf, login[chn] ? 1:BUFSIZE);

                if (cnt > 0)
                {
                    //Recieve lines
                    int i;
                    for(i = 0; i < cnt; i++)
                      if (buf[i] == '\r')
                      {
                        if (login[chn])
                        {
                            login[chn] = false;
                            status();
                            char call[10]; call[9] = '\0';
                            strncpy(call, line[chn], 9);
                            strcpy(line[chn], "");
                            normalize_call(call);
                            //simulate the connection
                            set_channel_state(chn, ST_CONN);
                            lp_set_var(chn, "_cwit", call);
                            lp_set_var(chn, "_cphy", call);
                            lp_emit_event(chn, EV_STATUS_CHANGE, ST_CONN, NULL);
                            lp_emit_event(chn, EV_CONN_TO, 1, call);
                        }
                        else
                        {
                            strcpy(sline[chn], line[chn]);
                            strcpy(line[chn], "");
                            lp_emit_event(chn, EV_LINE_RECV, 0, sline[chn]);
                        }
                      }
                      else
                        if (strlen(line[chn]) < LINE_LEN)
                          strncat(line[chn], &buf[i], 1);

                    //Emit the whole buffer
                    if (!login[chn])
                        lp_emit_event(chn, EV_DATA_INPUT, cnt, buf);
                }
                else if (cnt == 0 && !login[chn])
                {
                    disconnect(chn);
                }
            }
    }
}

//-------------------------------------------------------------------------

/* user event handler */
void my_handler(Event *ev)
{
   switch (ev->type)
   {
      case EV_ABORT:
          if (ev->chn == lp_channel() &&
              (ev->data == NULL || strcasecmp((char *)ev->data, ABORT_ADDR) == 0))
              abort_all = 1;
          break;
      case EV_DISC_LOC:
          if (sock[ev->chn] != -1) disconnect(ev->chn);
          break;
      case EV_DATA_OUTPUT:
      case EV_LOCAL_MSG:
          if (sock[ev->chn] != -1)
              data_output(ev->chn, (char *)ev->data, ev->x);
          break;
   }
}

//------------------------------------------------------------------------

void init(int channels)
{

   sock = new int[channels];
   line = new char*[channels];
   sline = new char*[channels];
   login = new bool[channels];

   for (int i = 0; i < channels; i++)
   {
      sock[i] = -1;
      line[i] = new char[LINE_LEN+1];
      strcpy(line[i], "");
      sline[i] = new char[LINE_LEN+1];
      strcpy(sline[i], "");
      login[i] = false;
   }
}

int main(int argc, char **argv)
{
   setbuf(stdout, NULL);
   if (lp_start_appl())
   {
      channels = lp_iconfig("maxchn") + 1;
      if (channels < 2)
      {
         fprintf(stderr, "lpapp: linpac returns 0 active channels?!\n");
         return 1;
      }

      init(channels);

      if (argc != 3)
      {
         lp_appl_result("Usage: ipcontrol <port> <callsign>");
         return 1;
      }
 
      char *endptr;
      portnum = strtol(argv[1], &endptr, 10);
      if (*endptr != '\0')
      {
         lp_appl_result("Invalid port number");
         return 1;
      }
 
      strncpy(mycall, argv[2], 19);
      normalize_call(mycall);
 
      status();
      lp_set_event_handler(my_handler);
 
      list = init_socket(portnum);
      if (list == -1)
      {
          lp_appl_result("Cannot initialize (see errorlog)");
          exit(1);
      }
      maxsock = list;
      while (!abort_all)
      {
          wait_data();
      }
      close(list);
      lp_remove_statline();
      lp_end_appl();
   }
   else printf("LinPac is not running\n");
   return 0;
}

