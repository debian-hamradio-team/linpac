#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "lpcalls.h"

int call_ssid(const char *src)
{
  char *p = const_cast<char *>(src);
  while (*p != '\0' && *p != '-') p++;
  if (*p) p++;
  if (*p) return atoi(p);
     else return 0;
}

char *call_call(const char *src)
{
  static char s[15];
  char *p = const_cast<char *>(src);
  strcpy(s, "");
  while (*p && isalnum(*p) && strlen(s) < 6) {strncat(s, p, 1); p++;}
  return s;
}

char *strip_ssid(char *src)
{
   if (call_ssid(src) == 0)
   {
      char *p = strchr(src, '-');
      if (p) *p = '\0';
   }
   return src;
}

char *normalize_call(char *call)
{
   char c[8];
   int ssid;
   char *p;

   strncpy(c, call_call(call), 6); c[6] = 0;
   ssid = call_ssid(call);
   if (ssid == 0) strcpy(call, c);
   else sprintf(call, "%s-%i", c, ssid);
   for (p = call; *p; p++) *p = toupper(*p);
   return call;
}

//Compare callsign only (not the SSID)
int compare_call(const char *call1, const char *call2)
{
   char c1[8];
   char c2[8];
   strcpy(c1, call_call(call1));
   strcpy(c2, call_call(call2));
   return (strcasecmp(c1, c2) == 0);
}

//Compare callsign and SSID
int compare_call_ssid(const char *call1, const char *call2)
{
   char c1[8];
   char c2[8];
   strcpy(c1, call_call(call1));
   strcpy(c2, call_call(call2));
   return (call_ssid(call1) == call_ssid(call2) &&
           strcasecmp(c1, c2) == 0);
}

